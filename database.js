const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('swagger-express-sequelize-node-sqlite', 'username', 'password', {
    dialect: 'sqlite',
    host: './database.sqlite'
})

module.exports = sequelize;