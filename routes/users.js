var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStratesy = require('passport-local');
var crypto = require('crypto');
var User = require('../models/user.model');


passport.use(new LocalStratesy(async function verify(username, password, done) {
  try {
    const user = await User.findOne({ where: { username: username } });
    if (!user) {
      return done(null, false, { message: 'Incorrect Username or Pssword.' })
    }
    crypto.pbkdf2(req.body.password, salt, 310000, 32, sha256, async function (error, hashedPassword) {
      if (err) {
        return done(err);
      }
      if (!crypto.timingSafeEqual(user.password, hashedPassword)) {
        return done(null, false, { message: 'Incorrect Username or Pssword.' })
      }
      return done(null, user)
    })
  } catch (e) {
    return done(e);
  }
}))

/* users home */
router.get('/home', (req, res, next) => {
  res.render('home');
});

/* users login */
router.get('/login', (req, res, next) => {
  res.render('login');
});

/* users login */
router.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/login?failed=1'
}));

/* users signup */
router.get('/signup', (req, res, next) => {
  res.render('signup');
});

/* users signup */
router.post('/signup', (req, res, next) => {
  let salt = crypto.randomBytes(16);
  crypto.pbkdf2(req.body.password, salt, 310000, 32, sha256, async function (error, hashedPassword) {
    if (err) {
      res.redirect('/signup?failed=1')
    }
    try {
      const user = await User.create({
        username: req.body.username,
        password: hashedPassword,
        salt: salt
      })

      req.login(user, function (err) {
        if (err) {
          res.redirect('signup?failed=3')
        } res.redirect('/');
      })

    } catch (e) {
      res.render('signup?failed=2')
    }
  })
});

module.exports = router;
